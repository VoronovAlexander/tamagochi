package com.example.aleksandrvoronov.tamagochi;

import android.app.Activity;
import android.os.Bundle;
import android.os.SystemClock;
import android.os.WorkSource;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.telephony.mbms.MbmsErrors;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import org.w3c.dom.Text;

import java.util.Timer;
import java.util.TimerTask;

public class GameActivity extends AppCompatActivity implements View.OnClickListener {

    TextView lvlName;
    ProgressBar energyBar, satietyBar, stressBar, pointsBar;
    Button eatShavermaButton, eatDoshikButton, drinkCoffeeButton, doSleepButton;
    ImageView imageView;

    Game game;
    Timer timer;

    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_game);

        lvlName = (TextView) findViewById(R.id.lvlName);
        energyBar = (ProgressBar) findViewById(R.id.energyBar);
        satietyBar = (ProgressBar) findViewById(R.id.satietyBar);
        stressBar = (ProgressBar) findViewById(R.id.stressBar);
        pointsBar = (ProgressBar) findViewById(R.id.pointsBar);

        imageView = (ImageView) findViewById(R.id.imageView);

        eatShavermaButton = (Button) findViewById(R.id.shavermaButton);
        eatDoshikButton = (Button) findViewById(R.id.doshikButton);
        drinkCoffeeButton = (Button) findViewById(R.id.coffeeButton);
        doSleepButton = (Button) findViewById(R.id.sleepButton);

        eatShavermaButton.setOnClickListener(this);
        eatDoshikButton.setOnClickListener(this);
        drinkCoffeeButton.setOnClickListener(this);
        doSleepButton.setOnClickListener(this);

        timer = new Timer();
        game = new Game();
        timer.schedule(game, 1000, 1000);
    }


    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.shavermaButton:
                game.eatShaverma();
                break;
            case R.id.doshikButton:
                game.eatDoshik();
                break;
            case R.id.coffeeButton:
                game.drinkCoffee();
                break;
            case R.id.sleepButton:
                game.doSleep();
                break;
        }
    }

    enum STATES_NAMES {WORKING, EATING_SHAVERMA, EATING_DOSHIK, DRINKING_COFFEE, SLEEPING}

    class State {
        public STATES_NAMES Name;
        private int BlockedSteps;
        private int ChangeEnergy;
        private int ChangeSatiety;
        private int ChangeStress;

        public int getBlockedSteps() {
            return BlockedSteps;
        }

        public int getChangeEnergy() {
            return ChangeEnergy;
        }

        public int getChangeSatiety() {
            return ChangeSatiety;
        }

        public int getChangeStress() {
            return ChangeStress;
        }

        public State(STATES_NAMES Name, int BlockedSteps, int ChangeEnergy, int ChangeSatiety, int ChangeStress) {
            this.Name = Name;
            this.BlockedSteps = BlockedSteps;
            this.ChangeEnergy = ChangeEnergy;
            this.ChangeSatiety = ChangeSatiety;
            this.ChangeStress = ChangeStress;
        }
    }

    class LVL {
        private String Name;
        private int Points;
        private int Image;

        public String getName() {
            return Name;
        }

        public int getPoints() {
            return Points;
        }

        public int getImage() { return Image; }

        public LVL(String Name, int Points, int Image) {
            this.Name = Name;
            this.Points = Points;
            this.Image = Image;
        }
    }

    public class Game extends TimerTask {

        private int Energy = 50;
        private int Satiety = 50;
        private int Stress = 50;
        private int Points = 0;

        private int BlockedSteps = 0;

        private State State;
        private State WORKING;
        private State SLEEPING;
        private State DRINKING_COFFEE;
        private State EATING_SHAVERMA;
        private State EATING_DOSHIK;

        private LVL[] LVLs;
        private LVL CurrentLvl;
        private int NumLVL;

        public Game() {

            this.WORKING = new State(STATES_NAMES.SLEEPING, 0, -2, -1, +1);
            this.SLEEPING = new State(STATES_NAMES.SLEEPING, 30, +3, -1, -2);
            this.DRINKING_COFFEE = new State(STATES_NAMES.DRINKING_COFFEE, 5, +5, +1, +7);
            this.EATING_SHAVERMA = new State(STATES_NAMES.EATING_SHAVERMA, 20, 0, +5, -2);
            this.EATING_DOSHIK = new State(STATES_NAMES.EATING_DOSHIK, 5, 0, +2, +1);
            this.State = this.WORKING;

            LVL[] CacheLVLs = {
                    new LVL("Junior", 100, R.drawable.i12),
                    new LVL("Middle", 300, R.drawable.i4),
                    new LVL("Senior", 500, R.drawable.i7),
                    new LVL("Master Yoda", 77777, R.drawable.i11)
            };
            LVLs = CacheLVLs;
            NumLVL = 0;
            this.setLvl(NumLVL);
        }

        void setLvl(int lvlNum) {
            this.CurrentLvl = LVLs[lvlNum];
            Points = 0;
            pointsBar.post(new Runnable() {
                public void run() {
                    lvlName.setText(CurrentLvl.getName());
                    pointsBar.setProgress(0);
                    pointsBar.setMax(CurrentLvl.getPoints());
                    imageView.setImageResource(CurrentLvl.getImage());
                }
            });
        }


        public void doSleep() {
            this.setState(SLEEPING);
        }

        public void drinkCoffee() {
            this.setState(DRINKING_COFFEE);
        }

        public void eatShaverma() {
            this.setState(EATING_SHAVERMA);
        }

        public void eatDoshik() {
            this.setState(EATING_DOSHIK);
        }

        private void setState(State newState) {

            if (BlockedSteps == 0 && State != WORKING) {
                Log.i("lifecycle", "Change to WORKING");
                State = WORKING;
                return;
            }

            if (State == WORKING && newState != WORKING) {
                State = newState;
                BlockedSteps = newState.getBlockedSteps();
            }

        }

        private void setStates(State State) {

            setState(State);

            int Energy = this.Energy + this.State.getChangeEnergy();
            if (Energy > 100) Energy = 100;
            else if (Energy <= 0) {
                energyBar.setProgress(0);
                Log.i("lifecycle", "Отключен от сети");
                Toast.makeText(GameActivity.this, "Отключен от сети", Toast.LENGTH_LONG).show();
                timer.cancel();
            }
            this.Energy = Energy;

            int Satiety = this.Satiety + this.State.getChangeSatiety();
            if (Satiety > 100) Satiety = 100;
            else if (Satiety <= 0) {
                satietyBar.setProgress(0);
                Log.i("lifecycle", "Умер с голода");
                Toast.makeText(GameActivity.this, "Умер с голода", Toast.LENGTH_LONG).show();
                timer.cancel();
            }
            this.Satiety = Satiety;

            int Stress = this.Stress + this.State.getChangeStress();
            if (Stress < 0) Stress = 0;
            else if (Stress >= 100) {
                stressBar.setProgress(100);
                Log.i("lifecycle", "Слетел с катушек");
                Toast.makeText(GameActivity.this, "Слетел с катушек", Toast.LENGTH_LONG).show();
                timer.cancel();
            }
            this.Stress = Stress;

            if (Energy > 80 && Satiety > 80 && Stress < 50) {
                Points += 10;
                if (Points > CurrentLvl.getPoints()) {
                    NumLVL++;
                    setLvl(NumLVL);
                }
            }

            Log.i("lifecycle", String.format("BlockedSteps: %d;Energy: %d; satiety: %d; Stress: %d", this.BlockedSteps, Energy, Satiety, Stress));

            energyBar.setProgress(Energy);
            satietyBar.setProgress(Satiety);
            stressBar.setProgress(Stress);
            pointsBar.setProgress(Points);

        }

        public void run() {
            GameActivity.this.runOnUiThread(new Runnable() {
                public void run() {

                    if (BlockedSteps > 0) {
                        BlockedSteps -= 1;
                    }

                    setStates(State);
                }
            });
        }

    }


}
